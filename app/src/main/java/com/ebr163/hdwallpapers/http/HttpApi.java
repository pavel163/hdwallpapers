package com.ebr163.hdwallpapers.http;

import com.ebr163.hdwallpapers.model.Wallpaper;

import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by Bakht on 20.12.2015.
 */
public interface HttpApi {

    @GET("/wallpapers/{category}/getNewWallpaper.php")
    Observable<Wallpaper> getNewWallpaper(@Path("category") String category, @Query("number") int number);
}
