package com.ebr163.hdwallpapers.http;

import android.content.Context;
import android.util.Log;

import com.ebr163.hdwallpapers.helper.SettingsHelper;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Bakht on 20.12.2015.
 */
public class HttpService {

    private HttpApi httpApi;

    private static HttpService instance;

    public static HttpService getInstance() {
        if (instance == null) {
            synchronized (HttpService.class) {
                if (instance == null) {
                    instance = new HttpService();
                }
            }
        }
        return instance;
    }

    private HttpService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://ebr163.esy.es")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        httpApi = retrofit.create(HttpApi.class);
    }

    public void subscribeAction(final Context context, Action1 action, Func1 error, Object query) {
        Observable observable = httpApi.getNewWallpaper(SettingsHelper.getCategory(context), (Integer) query);

        if (observable != null) {
            observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .onErrorResumeNext(error)
                    .subscribe(action, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            Log.d("xaxa","xaxa");
                        }
                    });
        }
    }

}
