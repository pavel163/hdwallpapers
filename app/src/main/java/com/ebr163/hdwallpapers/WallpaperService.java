package com.ebr163.hdwallpapers;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.ebr163.hdwallpapers.database.CRUDService;
import com.ebr163.hdwallpapers.helper.SettingsHelper;
import com.ebr163.hdwallpapers.http.HttpService;
import com.ebr163.hdwallpapers.model.Wallpaper;
import com.ebr163.hdwallpapers.reciever.ScreenManagerReceiver;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by pavel on 21.12.15.
 */
public class WallpaperService {

    private String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/hdwallpapers";

    public void getImage(final Context context, final SimpleTarget<Bitmap> simpleTarget, final ScreenManagerReceiver managerReceiver) {
        Action1<Wallpaper> onNextAction = new Action1<Wallpaper>() {
            @Override
            public void call(Wallpaper wallpaper) {
                if (wallpaper != null) {
                    setWallpaper(wallpaper.wallpaper, context, simpleTarget);
                    SettingsHelper.saveNumber(context, wallpaper.status + 1, wallpaper.wallpaper);
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                managerReceiver.offNotification(context);
                Log.d("adas", "adasd");
                return null;
            }
        };

        if (SettingsHelper.getOffline(context) == -1) {
            HttpService.getInstance().subscribeAction(context, onNextAction, error, SettingsHelper.getNumberForCategory(context, SettingsHelper.getCategory(context)));
        } else {
            CRUDService crudService = ((BaseApplication)context).crudService;
            Wallpaper wallpaper = crudService.getWallpaperById(SettingsHelper.getOffline(context));
            setWallpaper(file_path + "/"+wallpaper.wallpaper, context, simpleTarget);
            if (wallpaper.status == crudService.getAllWallpaper().size()){
                SettingsHelper.saveOffline(context, 1);
            } else {
                SettingsHelper.saveOffline(context, wallpaper.status + 1);
            }
        }

    }

    public void setWallpaper(final String url, Context context, SimpleTarget<Bitmap> simpleTarget) {
        Glide.with(context)
                .load(url)
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(simpleTarget);
    }

    public void setImage(Context context, ImageView view, String url) {
        Glide.with(context)
                .load(url)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.drawable.no_image)
                .crossFade()
                .into(view);
    }

    public void saveWallpaper(Context context, ProgressDialog progressDialog) {
        setWallpaper(SettingsHelper.getUrlForCategory(context), context, createSimpleTarget(context, progressDialog));
    }

    public void onOffline(Context context) {
        File dir = createDir();

        CRUDService crudService = ((BaseApplication)context).crudService;
        int i = 1;
        for (File f : dir.listFiles()) {
            if (f.isFile()) {
                Wallpaper wallpaper = new Wallpaper();
                wallpaper.status = i;
                wallpaper.wallpaper = f.getName();
                i++;

                crudService.put(wallpaper);
            }
        }
    }

    public File createDir(){
        File dir = new File(file_path);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dir;
    }

    public void offOffline(Context context) {
        CRUDService crudService = ((BaseApplication)context).crudService;
        crudService.deleteAllWallpaper();
    }

    private SimpleTarget<Bitmap> createSimpleTarget(final Context context, final ProgressDialog progressDialog) {
        return new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                File dir = new File(file_path);
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                String category = SettingsHelper.getCategory(context);

                File file = new File(dir, category + "_" + SettingsHelper.getNumberForCategory(context, category) + ".jpg");
                FileOutputStream fOut = null;
                try {
                    fOut = new FileOutputStream(file);
                    resource.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                    fOut.flush();
                    fOut.close();
                    if (progressDialog != null)
                        progressDialog.dismiss();
                    ((MainActivity) context).showMessage("Изображение сохранено");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
    }
}
