package com.ebr163.hdwallpapers.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.ebr163.hdwallpapers.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bakht on 20.12.2015.
 */
public class SettingsHelper {

    public static final String SETTINGS = "settings";
    public static final String STATE = "state";
    public static final String CATEGORY = "category";
    public static final String OFFLINE = "offline";
    public static final String URL = "url";

    public static List<String> categoryName = new ArrayList<>();
    public static List<String> categoryOriginName = new ArrayList<>();

    public static void saveNumber(Context context, int number, String url) {
        SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(getCategory(context), number);
        editor.putString(URL, url);
        editor.apply();
    }

    public static int getNumberForCategory(Context context, String category) {
        SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        return preferences.getInt(category, 1);
    }

    public static String getUrlForCategory(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        return preferences.getString(URL, "http://ebr163.esy.es/wallpapers/nature/2.jpg");
    }

    public static void saveState(Context context, boolean state) {
        SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(STATE, state);
        editor.apply();
    }

    public static boolean getState(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        return preferences.getBoolean(STATE, false);
    }

    public static void saveCategory(Context context, String category) {
        SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(CATEGORY, category);
        editor.apply();
    }

    public static String getCategory(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        return preferences.getString(CATEGORY, "nature");
    }

    public static void saveOffline(Context context, int id) {
        SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(OFFLINE, id);
        editor.apply();
    }

    public static int getOffline(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        return preferences.getInt(OFFLINE, -1);
    }

    public static void fullListCategory(Context context){
        if(categoryName.size() != 0){
            return;
        }

        categoryName.add(context.getString(R.string.auto));
        categoryName.add(context.getString(R.string.anime));
        categoryName.add(context.getString(R.string.girls));
        categoryName.add(context.getString(R.string.games));
        categoryName.add(context.getString(R.string.cats));
        categoryName.add(context.getString(R.string.minimum));
        categoryName.add(context.getString(R.string.men));
        categoryName.add(context.getString(R.string.nature));
        categoryName.add(context.getString(R.string.other));
        categoryName.add(context.getString(R.string.dogs));
        categoryName.add(context.getString(R.string.films));
        categoryName.add(context.getString(R.string.football));

        categoryOriginName.add("auto");
        categoryOriginName.add("anime");
        categoryOriginName.add("girls");
        categoryOriginName.add("games");
        categoryOriginName.add("cats");
        categoryOriginName.add("minimum");
        categoryOriginName.add("men");
        categoryOriginName.add("nature");
        categoryOriginName.add("other");
        categoryOriginName.add("dogs");
        categoryOriginName.add("films");
        categoryOriginName.add("football");
    }

}
