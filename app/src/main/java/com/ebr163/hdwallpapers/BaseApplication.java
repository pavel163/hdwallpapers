package com.ebr163.hdwallpapers;

import android.app.Application;

import com.ebr163.hdwallpapers.database.CRUDService;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKAccessTokenTracker;
import com.vk.sdk.VKSdk;

/**
 * Created by pavel on 22.12.15.
 */
public class BaseApplication extends Application {

    VKAccessTokenTracker vkAccessTokenTracker = new VKAccessTokenTracker() {
        @Override
        public void onVKAccessTokenChanged(VKAccessToken oldToken, VKAccessToken newToken) {

        }
    };

    public CRUDService crudService;

    @Override
    public void onCreate() {
        super.onCreate();
        WallpaperService wallpaperService = new WallpaperService();
        wallpaperService.createDir();
        vkAccessTokenTracker.startTracking();
        VKSdk.initialize(this);
        crudService = new CRUDService(this);
    }
}
