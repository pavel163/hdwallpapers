package com.ebr163.hdwallpapers.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.ebr163.hdwallpapers.R;

/**
 * Created by Bakht on 27.12.2015.
 */
public abstract class AbstractActivity extends AppCompatActivity{

    protected Toolbar toolbar;
    protected TextView toolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void initToolbar(String title){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        toolbar.setTitle("");
        toolbarTitle.setText(title);
        setSupportActionBar(toolbar);
    }
}
