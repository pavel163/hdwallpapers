package com.ebr163.hdwallpapers;

import android.graphics.Bitmap;
import android.widget.Toast;

import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.vk.sdk.VKScope;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.photo.VKImageParameters;
import com.vk.sdk.api.photo.VKUploadImage;

/**
 * Created by pavel on 23.12.15.
 */
public class VKService {

    private String[] vkScope = {VKScope.WALL, VKScope.PAGES};


    private SimpleTarget<Bitmap> createSimpleTarget(){
        return new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                VKRequest request = VKApi.wall().post(VKParameters.from(VKApiConst.MESSAGE, "Мои обои из HDWallpapers"));
                request.attempts = 1;
                request = VKApi.uploadWallPhotoRequest(new VKUploadImage(resource, VKImageParameters.jpgImage(0.9f)), 0, 60479154);
                request.executeWithListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        super.onComplete(response);
                       // Toast.makeText(MainActivity.this, "Опубликовано", Toast.LENGTH_LONG).show();
                    }
                });
            }
        };
    }
}
