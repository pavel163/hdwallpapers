package com.ebr163.hdwallpapers.settings;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.ebr163.hdwallpapers.R;
import com.ebr163.hdwallpapers.helper.SettingsHelper;

import java.util.List;

/**
 * Created by pavel on 22.12.15.
 */
public class SettingsDialog extends DialogFragment implements DialogInterface.OnClickListener  {

    private Spinner spinner;
    private OnClickDialogButton onClickDialogButton;

    public interface OnClickDialogButton{
        public void onDialogPositiveClick();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onClickDialogButton = (OnClickDialogButton) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnClickDialogButton");
        }
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater ltInflater = getActivity().getLayoutInflater();
        View view = ltInflater.inflate(R.layout.settings, null, false);
        spinner = (Spinner) view.findViewById(R.id.spinner);;
        spinner.setAdapter(getAdapter(SettingsHelper.categoryName));
        defaultSelect(spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SettingsHelper.saveCategory(getActivity(), SettingsHelper.categoryOriginName.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.categories)
                .setPositiveButton(R.string.ok, this)
                .setNegativeButton(R.string.cancel, this)
                .setView(view);
        return adb.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == Dialog.BUTTON_POSITIVE){
            onClickDialogButton.onDialogPositiveClick();
        }
    }

    private ArrayAdapter<String> getAdapter(List<String> data){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return adapter;
    }

    private void defaultSelect(Spinner spinner){
        int position = SettingsHelper.categoryOriginName.indexOf(SettingsHelper.getCategory(getActivity()));
        spinner.setSelection(position);
    }
}
