package com.ebr163.hdwallpapers.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.ebr163.hdwallpapers.model.Wallpaper;

import java.util.List;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Created by pavel on 28.12.15.
 */
public class CRUDService {

    private Context context;
    private SQLiteDatabase db;

    public CRUDService(Context context) {
        this.context = context;
        CupboardSQLiteOpenHelper cupboardSQLiteOpenHelper = new CupboardSQLiteOpenHelper(context);
        db = cupboardSQLiteOpenHelper.getWritableDatabase();
    }

    public long put(Object object) {
        return cupboard().withDatabase(db).put(object);
    }

    public List<Wallpaper> getAllWallpaper() {
        return cupboard().withDatabase(db).query(Wallpaper.class).list();
    }

    public Wallpaper getWallpaperById(int id) {
        return cupboard().withDatabase(db).query(Wallpaper.class).withSelection("status = ?", String.valueOf(id)).get();
    }

    public void deleteAllWallpaper() {
        cupboard().withDatabase(db).delete(Wallpaper.class, null);
    }
}
