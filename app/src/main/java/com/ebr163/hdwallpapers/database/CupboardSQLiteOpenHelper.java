package com.ebr163.hdwallpapers.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ebr163.hdwallpapers.model.Wallpaper;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Created by pavel on 28.12.15.
 */
public class CupboardSQLiteOpenHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "wallpapers.db";
    private static final int DATABASE_VERSION = 1;

    static {
        cupboard().register(Wallpaper.class);
    }

    public CupboardSQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        cupboard().withDatabase(db).createTables();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        cupboard().withDatabase(db).upgradeTables();
    }
}
