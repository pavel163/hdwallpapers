package com.ebr163.hdwallpapers.reciever;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.WallpaperManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.app.NotificationCompat;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.ebr163.hdwallpapers.MainActivity;
import com.ebr163.hdwallpapers.R;
import com.ebr163.hdwallpapers.WallpaperService;
import com.ebr163.hdwallpapers.helper.SettingsHelper;

import java.io.IOException;
import java.util.concurrent.Executor;

public class ScreenManagerReceiver extends BroadcastReceiver {

    private static boolean flag = true;
    private NotificationManager notificationManager;
    private WallpaperService wallpaperService;

    public ScreenManagerReceiver() {
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        if (!SettingsHelper.getState(context)) {
            return;
        }

        notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        if (flag) {
            flag = false;
            notificationManager.notify(1, createNotification(context, "Загрузка обоев..."));
            wallpaperService = new WallpaperService();
            wallpaperService.getImage(context.getApplicationContext(), createSimpleTarget(context), this);
        }

        Executor executor = new ApplicationExecutor();
        ((ApplicationExecutor) executor).executeDelayed(new Runnable() {
            @Override
            public void run() {
                flag = true;
            }
        }, 1000);
    }

    private SimpleTarget<Bitmap> createSimpleTarget(final Context context) {
        return new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                WallpaperManager wallpaperManager = WallpaperManager.getInstance(context);
                wallpaperManager.getDesiredMinimumWidth();
                wallpaperManager.getDesiredMinimumHeight();
                try {
                    WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                    DisplayMetrics metrics = new DisplayMetrics();
                    wm.getDefaultDisplay().getMetrics(metrics);
                    int height = resource.getHeight();
                    int width = resource.getWidth();

                    float scaleWidth;
                    float scaleHeight;

                    if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                        scaleWidth = (float) metrics.heightPixels / resource.getHeight();
                        scaleHeight = (float) metrics.widthPixels / resource.getWidth();
                    } else {
                        scaleWidth = (float) metrics.widthPixels / resource.getHeight();
                        scaleHeight = (float) metrics.heightPixels / resource.getWidth();
                    }


                    Matrix matrix = new Matrix();
                    if (scaleHeight < 1 && scaleWidth < 1) {
                        matrix.postScale(scaleWidth, scaleHeight);
                        wallpaperManager.setBitmap(Bitmap.createBitmap(resource, 0, 0, width, height, matrix, true));
                    } else {
                        wallpaperManager.setBitmap(resource);
                    }

                    notificationManager.notify(1, createNotification(context, "Загружено"));

                    sendMessage(context);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private void sendMessage(Context context) {
        Intent intent = new Intent(MainActivity.BROADCAST_ACTION);
        context.sendBroadcast(intent);
    }

    private Notification createNotification(Context context, String message) {
        return new NotificationCompat.Builder(context)
                .setContentTitle("HDWalpapers")
                .setContentText(message)
                .setSmallIcon(R.mipmap.ic_launcher)
                .build();
    }

    public void offNotification(Context context) {
        notificationManager.notify(1, createNotification(context, "Нет доступа к сети. Вы можете включить оффлайн режим"));
    }

    private static class ApplicationExecutor implements Executor {
        final Handler handler = new Handler(Looper.getMainLooper());

        public void executeDelayed(@NonNull Runnable command, long delay) {
            handler.postDelayed(command, delay);
        }

        @Override
        public void execute(@NonNull Runnable command) {
            handler.post(command);
        }
    }
}
