package com.ebr163.hdwallpapers;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebr163.hdwallpapers.base.AbstractActivity;
import com.ebr163.hdwallpapers.helper.SettingsHelper;
import com.ebr163.hdwallpapers.settings.SettingsDialog;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import java.io.File;

public class MainActivity extends AbstractActivity implements View.OnClickListener, SettingsDialog.OnClickDialogButton {

    public static final String UPDATE_IMAGE = "update";
    public final static String BROADCAST_ACTION = "com.ebr163.hdwallpapers.update";

    private WallpaperService wallpaperService;
    private ImageView imageView;
    private FloatingActionButton fab;
    private TextView categoryView;
    private CoordinatorLayout coordinatorLayout;
    private ProgressDialog progressDialog;
    private BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SettingsHelper.fullListCategory(this);
        wallpaperService = new WallpaperService();
        initUI();
        registerBroadcast();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
             //   wallpaperService.getImage(MainActivity.this, createSimpleTarget());
            }

            @Override
            public void onError(VKError error) {}
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem menuItem = menu.findItem(R.id.action_settings);
        menuItem.setChecked(SettingsHelper.getOffline(this) == -1 ? false : true );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_settings:
                if (SettingsHelper.getOffline(this) == -1){
                    item.setChecked(true);
                    SettingsHelper.saveOffline(this, 1);
                    wallpaperService.onOffline(getApplicationContext());
                }else {
                    item.setChecked(false);
                    SettingsHelper.saveOffline(this, -1);
                    wallpaperService.offOffline(getApplicationContext());
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initUI() {
        initToolbar(getResources().getString(R.string.app_name));
        imageView = (ImageView) findViewById(R.id.image);
        setImage();
        fab = (FloatingActionButton) findViewById(R.id.fab);
        setFABColor();
        categoryView = (TextView) findViewById(R.id.category);
        setCategory();
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator);
    }

    private void setCategory(){
        int position = SettingsHelper.categoryOriginName.indexOf(SettingsHelper.getCategory(this));
        categoryView.setText(SettingsHelper.categoryName.get(position));
    }

    private void setFABColor(){
        if(!SettingsHelper.getState(this)){
            fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
        }else{
            fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccentTwo)));
        }
    }

    private void setImage(){
        String url = SettingsHelper.getUrlForCategory(this);
        if (!url.equals("")) {
            wallpaperService.setImage(this, imageView, url);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                SettingsHelper.saveState(this, !SettingsHelper.getState(this));
                setFABColor();
                if(SettingsHelper.getState(this)){
                    showMessage("Автоматическая смена обоев включена");
                }else{
                    showMessage("Автоматическая смена обоев выключена");
                }
                break;
            case R.id.save:
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Сохранение");
                progressDialog.setCancelable(false);
                progressDialog.show();
                wallpaperService.saveWallpaper(this, progressDialog);
                //VKSdk.login(this, vkScope);
                break;
            case R.id.category_set:
                DialogFragment dialogFragment = new SettingsDialog();
                dialogFragment.show(getSupportFragmentManager(), "settings");
                break;
        }
    }

    private void registerBroadcast(){
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                setImage();
            }
        };
        IntentFilter intFilt = new IntentFilter(BROADCAST_ACTION);
        registerReceiver(broadcastReceiver, intFilt);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onDialogPositiveClick() {
        setCategory();
    }

    public void showMessage(String message){
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }
}
